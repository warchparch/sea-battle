import random
field ={}
ships = {}
letters = 'abcdefghij'

for item1 in letters:
    for item2 in range(1,11):
        field[item1+str(item2)] = 0
playable_field = field.copy()

#prints the field
def print_field(field):
    print('    a b c d e f g h i j')
    print('    -------------------')
    number = 0
    for item2 in range(1,11):
        number += 1
        if number != 10:
            print(number,'','|', end="")
        else:
            print("10 |",end='')
        for item1 in letters:
            print(field[item1+str(item2)], end=' ')
        print('')

#returns a ship of decks length
def create_a_ship(decks):
    changes=False
    # attempt = 0
    while changes == False:
        # attempt += 1
        start = str(letters[random.randint(0,10)-1]+str(random.randint(1,10)))
        if field[start] == 0:
            random_number = random.randint(0,1)
            if random_number == 0:
                changes = search_horizontal(start, decks)
            else:
                changes = search_vertical(start,decks)
            if changes:
                for item in changes:
                    field[item] = 1
                for item in make_borders(changes):
                    field[item] = '.'
    # print('attempts: ', attempt)
    return [changes, make_borders(changes)]

#returns a horizontal ship of decks length from the start point
def search_horizontal(start,decks):
    last_tile = start
    found =[start]
    for item in range(decks-1):
        if int(last_tile[1:])<10:
            if field[last_tile[:1] + str(int(last_tile[1:]) + 1)] == 0:
                found.append(last_tile[:1] + str(int(last_tile[1:]) + 1))
                last_tile = last_tile[:1] + str(int(last_tile[1:]) + 1)
    if len(found)==decks:
        return(found)
    else:
        return False

#returns a vertical ship of decks length from the start point
def search_vertical(start, decks):
    last_tile = start
    found = [start]
    for item in range(decks - 1):
        if letters.find(last_tile[:1]) < 9:
            if field[letters[letters.find(last_tile[:1])+1] + last_tile[1:]] == 0:
                found.append(letters[letters.find(last_tile[:1])+1] + last_tile[1:])
                last_tile = letters[letters.find(last_tile[:1])+1] + last_tile[1:]
    if len(found) == decks:
        return (found)
    else:
        return False

#returns unique elements of a list
def unique(seq):
   checked = []
   for e in seq:
       if e not in checked:
           checked.append(e)
   return checked

#makes borders of ships
def make_borders(found):
    exists = []
    for item in found:
        exists.append(item[:1]+str(int(item[1:]) + 1))
        exists.append(item[:1] + str(int(item[1:]) - 1))
        try:
            exists.append(letters[letters.find(item[:1])+1] + item[1:])
        except:
            pass
        try:
            exists.append(letters[letters.find(item[:1])-1] + item[1:])
        except:
            pass
        try:
            exists.append(letters[letters.find(item[:1])-1] + str(int(item[1:]) + 1))
        except:
            pass
        try:
            exists.append(letters[letters.find(item[:1])+1] + str(int(item[1:]) + 1))
        except:
            pass
        try:
            exists.append(letters[letters.find(item[:1])+1] + str(int(item[1:]) - 1))
        except:
            pass
        try:
            exists.append(letters[letters.find(item[:1])-1] + str(int(item[1:]) - 1))
        except:
            pass
    not_itself = []
    for item in exists:
        if item not in found:
            not_itself.append(item)
    return unique(not_itself)
#starts a new game
def start_the_game():
    while True:
        x = input('Enter your move or "exit"   ')
        if x == 'exit':
            break
        try:
            field[x]
        except:
            print('Not a number!')
        else:
            playable_field[x]=field[x]
            if field[x] == 1:
                if check_shut_down(x):
                    for item in check_shut_down(x):
                        playable_field[item] ='.'
                    print("You've shot down a ship!")

                else:
                    print("You've hit a ship!")
            else:
                playable_field[x] = field[x] = '.'
                print("You've missed!")
            print_field(playable_field)

#checks if a ship was shut down
def check_shut_down(x):
    for item in ships:
        if x in ships[item][0]:
            for item2 in ships[item][0]:
                if field[item2] != playable_field[item2]:
                    return False
            return ships[item][1]

#determination of ships
def create_the_field():
    counter = 0
    for item in range(1):
        ships[counter] = create_a_ship(4)
        counter+=1
    for item in range(2):
        ships[counter] = create_a_ship(3)
        counter += 1
    for item in range(3):
        ships[counter] = create_a_ship(2)
        counter += 1
    for item in range(4):
        ships[counter] = create_a_ship(1)
        counter += 1

print('')
create_the_field()
print_field(playable_field)
start_the_game()

